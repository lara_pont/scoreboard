import React from 'react'
import { Board } from './components'

const App = () => {
  return <Board />
}

export default App
