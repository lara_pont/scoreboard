import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Team, Scores } from 'components'

import styles from './Match.module.scss'

function Match ({ match }) {
  const { homeTeam, awayTeam, score } = match
  const [scoreHome, setScoreHome] = useState(0)
  const [scoreAway, setScoreAway] = useState(0)

  useEffect(() => {
    if (score !== undefined) {
      const result = score.split('-')
      setScoreHome(parseInt(result[0]))
      setScoreAway(parseInt(result[1]))
    } else {
      setScoreHome(0)
      setScoreAway(0)
    }
  }, [score])

  return (
    <div className={styles['match']}>
      <Team name={homeTeam} />
      <Scores scoreHome={scoreHome} scoreAway={scoreAway} />
      <Team name={awayTeam} />
    </div>
  )
}

Match.propTypes = {
  match: PropTypes.shape({
    id: PropTypes.number.isRequired,
    homeTeam: PropTypes.string.isRequired,
    awayTeam: PropTypes.string.isRequired
  })
}

export default Match
