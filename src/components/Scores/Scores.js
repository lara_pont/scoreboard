import React from 'react'
import PropTypes from 'prop-types'
import styles from './Scores.module.scss'

function Scores ({ scoreHome, scoreAway }) {
  return (
    <div className={styles['score']}>
      {scoreHome} - {scoreAway}
    </div>
  )
}

Scores.propTypes = {
  scoreHome: PropTypes.number,
  scoreAway: PropTypes.number
}

Scores.defaultProps = {
  scoreHome: 0,
  scoreAway: 0
}

export default Scores
