import React from 'react'
import PropTypes from 'prop-types'
import styles from './Team.module.scss'

function Team ({ name }) {
  return (
    <div className={styles['team']}>
      {name}
    </div>
  )
}

Team.propTypes = {
  name: PropTypes.string.isRequired
}

export default Team
