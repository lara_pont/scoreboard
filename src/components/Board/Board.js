import React, { useState, useEffect } from 'react'
import { Match } from 'components'

import state from 'data/state.json'
import styles from './Board.module.scss'

const Board = () => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    setData(sortScores(state))
    setLoading(false)
  }, [])

  function sortScores (myscores) {
    return myscores.sort(function (a, b) {
      const scoreA = a.score !== undefined ? a.score.split('-') : [0, 0]
      const totalScoreA = parseInt(scoreA[0]) + parseInt(scoreA[1])

      const scoreB = b.score !== undefined ? b.score.split('-') : [0, 0]
      const totalScoreB = parseInt(scoreB[0]) + parseInt(scoreB[1])

      if (totalScoreA === totalScoreB) {
        return a.id > b.id ? -1 : 1
      }

      return totalScoreB - totalScoreA
    })
  }

  return (
    <div className={styles['scoreboard']}>
      <h1>Scoreboard</h1>
      { loading && <p>Loading...</p> }
      {
        !loading && data.length > 0
          ? <div className={styles['matches']}>
            {data
              .map(item => (
                <Match key={item.id} match={item} />
              ))}
          </div>
          : <p>There are not matches yet.</p>
      }
    </div>
  )
}

export default Board
