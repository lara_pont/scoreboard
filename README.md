# Scoreboard App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting started

To get the frontend running locally:

- Clone this repo: `git clone git@bitbucket.org:lara_pont/scoreboard.git`
- `npm install` to install all required dependencies
- `npm start` to start the local server (this project uses create-react-app)

Open [http://localhost:3000](http://localhost:3000) to view it in the browser

## Data

The data can be found in the file state.json in the directory /data

Operations:

- If we don't add the property **score** to an item of the object, it means that the match has just started.
- If we add a score property it's because the result has changed.
- If the match is removed from the object it's because it has already ended. 

## Testing

In the project directory, you can run:

- `npm test`

Launches the test runner in the interactive watch mode.\



